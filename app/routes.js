const { exchangeRates } = require('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});

	app.get('/rates', (req, res) => {
		return res.send({
			rates: exchangeRates
		});
	})

	app.post('/currency', (req, res) => {
		if (!req.body.hasOwnProperty('name')) {
			return res.status(400).send({
				"error": "Bad Request: missing required parameter NAME"
			});
		}

		if (typeof req.body.name !== "string") {
		 	return res.status(400).send({
		  		"error": "Bad Request: Name has to be a string"
		  	});
		}

  		if (req.body.name === '') {
    		return res.status(400).send({
      			"error": "Bad Request: NAME cannot be empty"
   			});
  		}


		if (!req.body.hasOwnProperty('ex')) {
			return res.status(400).send({
				"error": "Bad Request: missing required parameter ex"
			});
		}


		if (typeof req.body.ex !== "object") {
		 	return res.status(400).send({
		  		"error": "Bad Request: ex is not cannot be an object"
		  	});
		}

		if (Object.keys(req.body.ex).length === 0) {
    		return res.status(400).send({
      			'error': 'Bad Request: ex cannot be empty'
		    });
		}

		if (!req.body.hasOwnProperty('alias')) {
			return res.status(400).send({
				"error": "Bad Request: missing property alias"
			});
		}



		if (typeof req.body.alias !== "string") {
		 	return res.status(400).send({
		  		"error": "Bad Request: alias has to be a string"
		  	});
		}


  		for (let currency in exchangeRates) {
   			if (exchangeRates.hasOwnProperty(currency)) {
     			if (exchangeRates[currency].alias === req.body.alias) {
        			return res.status(400).send({
          				"error": "Bad Request: duplicate alias"
			        });
			      }
			    }
				  }
	 	    return res.status(200).send({
	  			    "message": "Request is valid"
			        });


	});
};