const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', () => {
	// Test if /rates endpoint is running
	it('test_api_get_rates_is_running', (done) => {
		chai.request('http://localhost:5001')
			.get('/rates')
			.end((err, res) => {
				expect(res).to.not.equal(undefined);
				done();
			});
	});

	// Test if /rates endpoint returns status 200
	it('test_api_get_rates_returns_200', (done) => {
		chai.request('http://localhost:5001')
			.get('/rates')
			.end((err, res) => {
				expect(res.status).to.equal(200);
				done();
			});
	});

	// Test if /rates endpoint returns an object of size 5
	it('test_api_get_rates_returns_object_of_size_5', (done) => {
		chai.request('http://localhost:5001')
			.get('/rates')
			.end((err, res) => {
				expect(Object.keys(res.body.rates)).to.have.length(5); // Corrected the method name
				done();
			});
	});

	// Test if /currency endpoint returns status 200
	it('test_api_get_currency_returns_200', (done) => {
		chai.request('http://localhost:5001')
			.get('/currency')
			.end((err, res) => {
				expect(res).to.not.equal(undefined);
				done();
			});
	});


	/*Check if post /currency returns status 400 if name is not a string*/
	it('test_api_post_currency_returns_400_if_name_not_string', (done) => {
	 	chai.request('http://localhost:5001')
	    	.post('/currency')
	   		.type('json')
	    	.send({
	   			name: 123

	    	})
	   		.end((err, res) => {
	      		expect(res.status).to.equal(400);
	      		done();
	   		});
	});

/*	Check if post /currency returns status 400 if name is empty*/
	it('test_api_post_currency_returns_400_if_name_empty', (done) => {
  		chai.request('http://localhost:5001')
	  		.post('/currency')
	   		.type('json')
	    	.send({
	 		    ex: 7.03

	 		})
		    .end((err, res) => {
		      expect(res.status).to.equal(400);
		      done();
		    });
	});

/*	Check if post /currency returns status 400 if ex is missing*/
	it('test_api_post_currency_returns_400_if_ex_is_missing', (done) => {
  		chai.request('http://localhost:5001')
	  		.post('/currency')
	   		.type('json')
	    	.send({
	 		    name: "Japanese Yen",

	 		})
		    .end((err, res) => {
		      expect(res.status).to.equal(400);
		      done();
		    });
	});

/*Check if post /currency returns status 400 if ex is not an object*/
	it('test_api_post_currency_returns_400_if_name_not_string', (done) => {
	 	chai.request('http://localhost:5001')
	    	.post('/currency')
	   		.type('json')
	    	.send({
	   			name: 123,
	   			ex: 456

	    	})
	   		.end((err, res) => {
	      		expect(res.status).to.equal(400);
	      		done();
	   		});
	});

/*	Check if post /currency returns status 400 if ex is empty */
	it('test_api_post_currency_returns_400_if_ex_is_empty', (done) => {
  		chai.request('http://localhost:5001')
	  		.post('/currency')
	   		.type('json')
	    	.send({
	 		    ex: {}

	 		})
		    .end((err, res) => {
		      expect(res.status).to.equal(400);
		      done();
		    });
	});

/*	Check if post /currency returns status 400 if alias is missing */
	it('test_api_post_currency_returns_400_if_alias_is_missing', (done) => {
  		chai.request('http://localhost:5001')
	  		.post('/currency')
	   		.type('json')
	    	.send({

	 		    name: "Japanese Yen"

	 		})
		    .end((err, res) => {
		      expect(res.status).to.equal(400);
		      done();
		    });
	});


/*CCheck if post /currency returns status 400 if alias is not an string*/
	it('test_api_post_currency_returns_400_if_alias_is_not_an_string', (done) => {
	 	chai.request('http://localhost:5001')
	    	.post('/currency')
	   		.type('json')
	    	.send({

	    		alias: 123,
      			name: "Chinese Yuan",
      			ex: {
	      			'peso': 7.21,
	        		'usd': 0.14,
	        		'won': 168.85,
	       			'yen': 15.45
      			}

	    	})
	   		.end((err, res) => {
	      		expect(res.status).to.equal(400);
	      		done();
	   		});
	});

/*Check if post /currency returns status 400 if alias is empty*/
	it('test_api_post_currency_returns_400_if_alias_is_empty', (done) => {
	 	chai.request('http://localhost:5001')
	    	.post('/currency')
	   		.type('json')
	    	.send({

	    		alias: {},
      			name: "Chinese Yuan",
      			ex: {
	      			'peso': 7.21,
	        		'usd': 0.14,
	        		'won': 168.85,
	       			'yen': 15.45
      			}

	    	})
	   		.end((err, res) => {
	      		expect(res.status).to.equal(400);
	      		done();
	   		});
	});

/* Check if post /currency returns status 400 if all fields are complete but there is a duplicate alias */
	it('test_api_post_currency_returns_400_if_duplicate_alias', (done) => {
	  	chai.request('http://localhost:5001')
		    .post('/currency')
		    .type('json')
		    .send({

		      	alias: "peso",
		      	name: "philippine peso",

		    })
		    .end((err, res) => {
		     	expect(res.status).to.equal(400);
		      	done();
		    });
	});

/* Check if post /currency returns status 200 if all fields are complete and there are no duplicates */
	it("test_api_post_currency_returns_200_if_all_fields_are_complete_without_duplicates", (done) => {
	  	chai.request('http://localhost:5001')
		    .post('/currency')
		    .type('json')
		    .send({

		      	alias: "gold",
		      	name: "philippine gold",
		      	ex: {
			        'peso': 0.7,
			        'usd': 0.092,
			        'won': 10.3,
			        'yuan': 0.65
		      }
		    })
		    .end((err, res) => {
		     	expect(res.status).to.equal(200);
		      	done();
		    });
	});




});
